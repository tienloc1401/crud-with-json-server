import React, { useEffect, useState } from 'react';
import { Table, TableCell, TableRow, 
    TableHead, TableBody, 
    makeStyles, 
    Button, 
    TextField,

} from '@material-ui/core';
import { deleteUser ,getAllUsers } from '../service/api';
import { Link } from 'react-router-dom';
import { Input } from 'rsuite';

const useStyle = makeStyles({
    table: {
        width: '80%',
        margin: '50px 100px 100px 100px',
    },
    thead:{
        '& > *':{            
            fontSize: '24px'
        }
    },
    trow:{
        '& > *':{
            fontSize: '20spx'
        }
    }
})

const Home = () => {

    const classes = useStyle();

    const [users, setUsers] = useState([]);
    useEffect(() => {
        getUsers();
    }, [])

    const getUsers = async () =>{
        const response = await getAllUsers();
        console.log(response);
        setUsers(response.data);
    }

    const deleteData = async (id) => {
        await deleteUser(id);
        getUsers();
    }

    const [searchInput, setSearchInput] = useState("")

    const [filteredResults, setFilteredResults] = useState([]);

    // const filteredData = users.filter((user) => {
    //     return Object.values(user).join('').toLowerCase().includes(searchInput.toLowerCase())
    // })

    const searchItems = (searchValue) => {
        setSearchInput(searchValue)
        if (searchInput){
            const filteredData = users.filter((user) => {
                return user.name.toLowerCase().includes(searchInput.trim().toLowerCase())                
            })
            console.log(filteredData)
            setFilteredResults(filteredData)
        }
        else {
            setFilteredResults(users)
        }        
    }

    return (
        <React.Fragment>
            <TextField 
                id="outlined-search" 
                placeholder="Search... " 
                type="search" 
                align="center"  
                variant="outlined"              
                style={{margin: '10px 40px'}}
                onChange={(e) => {
                    searchItems(e.target.value)
                    console.log(e.target.value)
                }
                
                } /> 
                                          
        <Table className={classes.table}>            
            <TableHead>
                <TableRow className={classes.thead}>
                    <TableCell>Index</TableCell>
                    <TableCell>Name</TableCell>                  
                    <TableCell>Phone</TableCell>
                    <TableCell>Birthday</TableCell>
                    <TableCell>Action</TableCell>
                    <TableCell></TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
            {searchInput.length > 0 ? (
                filteredResults.map((data, index) => (
                    <TableRow className={classes.trow}>
                        <TableCell>{index + 1}</TableCell>
                        <TableCell>{data.name}</TableCell>                    
                        <TableCell>{data.phone}</TableCell>
                        <TableCell>{data.birth_day}</TableCell>
                        <TableCell>
                            <Button 
                                variant="contained" 
                                color="primary" 
                                style={{margin: '0px 10px'}} 
                                component={Link} to={`/edit/${data.id}`}
                            >
                                Edit
                            </Button>
                            <Button 
                                variant="contained" 
                                color="secondary" 
                                style={{margin: '0px 10px'}} onClick={() => deleteData(data.id)}>Delete</Button>
                        </TableCell>
                    </TableRow>
                    ))
                ): (
            

            users.map((data, index) => (
                    <TableRow className={classes.trow}>
                        <TableCell>{index + 1}</TableCell>
                        <TableCell>{data.name}</TableCell>                    
                        <TableCell>{data.phone}</TableCell>
                        <TableCell>{data.birth_day}</TableCell>
                        <TableCell>
                            <Button variant="contained" color="primary" style={{margin: '0px 10px'}} component={Link} to={`/edit/${data.id}`}>Edit</Button>
                            <Button variant="contained" color="secondary" style={{margin: '0px 10px'}} onClick={() => deleteData(data.id)}>Delete</Button>
                        </TableCell>
                    </TableRow>
                ))
                )
            }

            </TableBody>            
        </Table>
        </React.Fragment>
    )
}

export default Home;


