import React, { useState } from 'react';
import { Container, Typography, FormControl, FormLabel, TextField, InputLabel, Input, Box, FormGroup, Button } from '@material-ui/core';
import { addUser } from '../service/api';
import { useHistory } from 'react-router-dom';

const initialValue = {
    name: "",
    phone: "",
    birth_day: "",
}

const AddUser = () => {

  const [user, setUser] = useState(initialValue);
  const {name, phone, birth_day} = user;

  const history = useHistory();

  const onValueChange = (e) =>
  {
    // console.log(e);
    // console.log(e.target.value);
    setUser({...user, [e.target.name]: e.target.value});
    console.log(user);
  }

  const addUserDetails = async () =>{
    await addUser(user);
    history.push('/');
  }

  return (
    <Container maxWidth="sm">
      <Box my={5}>
        <Typography variant="h4" align="center">User Details</Typography>
        <FormGroup>

          <FormControl>
            <TextField        
            label="Name"
            type="text"
            onChange={(e) => onValueChange(e)} 
            name="name" 
            value={name}
            
            />
            {/* <InputLabel>Name</InputLabel>
            <Input onChange={(e) => onValueChange(e)} name="name" value={name} /> */}
          </FormControl>

          <FormControl>
            <TextField        
              label="Phone Number"
              type="text"
              onChange={(e) => onValueChange(e)} 
              name="phone" 
              value={phone}
              
              />
            {/* <InputLabel>Phone Number</InputLabel>
            <Input onChange={(e) => onValueChange(e)} name="phone" value={phone} /> */}
          </FormControl>

          <FormControl>
            <FormLabel>Birthday</FormLabel>
            <TextField
              onChange={(e) => onValueChange(e)} 
              name="birth_day" 
              type="date" 
              value={birth_day}
                />
          </FormControl>
          <Box my={3}>
            <Button onClick={()=> history.push("/")} variant="contained" color="secondary" align="center">Back</Button>
            <Button variant="contained" onClick={() => addUserDetails() } color="primary" align="center">Save</Button>
          </Box>
        </FormGroup>
      </Box>
    </Container>
  )
}

export default AddUser;