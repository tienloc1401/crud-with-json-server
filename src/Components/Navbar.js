import React from 'react';
import { AppBar, makeStyles, Toolbar } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

const useStyles = makeStyles({
    header: {
        backgroundColor: '#3474eb',
    },
    spacing :{
        paddingLeft: 20,
        color: '#fff',
        fontSize: '24px',
        textDecoration: 'none',
    }
  });

const Navbar = () => {
    const classes = useStyles();
    return (
        <AppBar className={classes.header} position="static">
            <Toolbar >
                <NavLink to="/" className={classes.spacing}>All Users</NavLink>
                <NavLink to="add" className={classes.spacing}>ADD</NavLink>
            </Toolbar>
        </AppBar>
    )
}

export default Navbar;