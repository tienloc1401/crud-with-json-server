import React, { useEffect, useState } from 'react';
import { Container, Typography, FormControl, InputLabel, Input, Box, FormGroup, Button } from '@material-ui/core';
import { editUser, getAllUsers } from '../service/api';
import { useHistory, useParams } from 'react-router-dom';

const initialValue = {
    name: "",
    phone: "",
    birth_day: "",
}

const EditUser = () => {

    const [user, setUser] = useState(initialValue);
    const {name, phone, birth_day} = user;

    const { id } = useParams();

    useEffect(() => {
        loadUserData();
    },[]);

    const loadUserData = async () =>{
        const response = await getAllUsers(id);
        setUser(response.data);
    }

    const history = useHistory();

    const onValueChange = (e) =>
    {
        // console.log(e);
        // console.log(e.target.value);
        setUser({...user, [e.target.name]: e.target.value});
        console.log(user);
    }

    const editUserDetails = async () =>{
       await editUser(id,user);
       history.push('/');
    }

    return (
        <Container maxWidth="sm">
            <Box my={3}>
            <Typography variant="h5" align="center">Edit User Details</Typography>
            <FormGroup>
                <FormControl>
                    <InputLabel>Name</InputLabel>
                    <Input onChange={(e) => onValueChange(e)} name="name" value={name} />
                </FormControl>
                <FormControl>
                    <InputLabel>Phone Number</InputLabel>
                    <Input onChange={(e) => onValueChange(e)} name="phone" value={phone} />
                </FormControl>
                <FormControl>
                    <InputLabel>Birthday</InputLabel>
                    <Input onChange={(e) => onValueChange(e)} name="birth_day" value={birth_day} type="date"/>
                </FormControl>
                <Box my={3}>
                    <Button onClick={()=> history.push("/")} variant="contained" color="secondary" align="center">Back</Button>
                    <Button variant="contained" onClick={() => editUserDetails() } color="primary" align="center">Save</Button>                    
                </Box>
            </FormGroup>
            </Box>
        </Container>
    )
}

export default EditUser;