import './App.css';
import Navbar from  './Components/Navbar';
import Home from './Components/Home';
import AddUser from './Components/AddUser';
import EditUser from './Components/EditUser';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/add" component={AddUser} exact />
        <Route path="/edit/:id" component={EditUser} exact />
      </Switch>
    </Router>
  );
}

export default App;
